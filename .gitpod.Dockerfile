FROM gitpod/workspace-full
SHELL ["/bin/bash", "-c"]
RUN \
  source ~/'.sdkman/bin/sdkman-init.sh' ;\
  sdk selfupdate force ;\
  yes | sdk update ;\
  yes | sdk install java '17.0.1-tem'
